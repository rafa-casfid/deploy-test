# deploy-test

## Introducción
Este es un proyecto que he creado para hacer pruebas con la pipeline de BitBucket y Cypress.

Las pipelines se definen en el fichero bitbucket-pipelines.yml. He eliminado la pipeline de la sección default porque prefiero que la pipeline se inicie manualmente y no al hacer un commit.

## Cypress

Los tests se encuentran dentro de la carpeta "tests". Sólo hay uno implementado y **estoy forzando un fallo** en el último paso del test para ver cómo la pipeline se detiene al ver que hay algo que no funciona con normalidad.

Puedes poner en marcha un contenedor Docker con Cypress preinstalado para ver la ejecución del test, así no tienes que instalar nada en tu sistema. Clona el proyecto e inicia el contenedor:
```
git clone https://rafa-casfid@bitbucket.org/rafa-casfid/deploy-test.git

sudo docker run -v $PWD/tests:/home/tests -w /home/tests cypress/included:3.2.0
```

### Detección del error por la pipeline

Para que Bitbucket Pipelines se entere de que el test ha fallado y, por tanto, no siga con los siguientes pasos de la pipeline, el sistema de testing debe producir un resultado que Bitbucket pueda interpretar y almacenarlo en un directorio concreto.
Actualmente, son capaces de detectar los errores reportados a través de ficheros XML de JUnit y Maven Surefire [https://support.atlassian.com/bitbucket-cloud/docs/test-reporting-in-pipelines].

Cypress es capaz de generar un informe en el formato de JUnit, así que es posible hacer funconar Cypress con la pipeline de Bitbucket. Tan sólo hay que configurar Cypress para que genere el informe  (ver cypress.json).

## Configuración de la pipeline

La pipeline simplemente está configurada para lenvantar un contenedor de Docker en el paso de Build and Test.

Para poner en marcha el proceso, ve a la sección de Pipelines, haz clic en "Run pipeline", selecciona la rama master y la única pipeline que hay. Cuando se ejecute, verás que el proceso está compuesto por varios pasos, pero el único que realmente hace algo es el de Build and Test, que pone en marcha el test de Cypress.