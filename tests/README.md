# Ejemplo de tests automáticos en aplicaciones web con CYPRESS

## Disclaimer

1. Los tests se han escrito en las 2h previas a la píldora formativa. Funcionan pero la forma de escribirlos no es la mejor, hay que **refactorizar** usando métodos, para no publicar tanto código en los tests.

2. Los **tests tienen que ser independientes** entre sí, para que si uno falla, podamos ir directamente a revisar ese test y volver a ejecutar solo ese test. 

3. Debido a la independencia de los tests, muchos comienzan con una parte similar (por ejemplo, muchos empiezan con login). Pero lo realmente correcto es que, un grupo de tests revisen el funcionamiento del login, pero el resto de **tests utilicen directamente una cookie de sesión almacenada o un access token obtenido vía API, en lugar de simular el formulario de login cada vez**. Así se consigue que los tests se ejecuten más rápido.

4. Cypress permite otras muchas funcionalidades, como **testear APIs, simular la respuesta de APIs, simular un código de error HTTP en la respuesta de la API, simular gestos del ratón (scroll, drag and drop, etc).**

5. Cuando escribas el HTML, recuerda añadir un id en los items, o mejor aún, añade el atrubuto **data-cy** en el elemento HTML. La selección de elementos del DOM es muy sencilla. Puedes buscarlos por id, clase, por texto que contienen... **cy.get() funciona como el operador $ de jquery**. Pero como todos estos atributos podrían cambiar si se retoca el diseño de la aplicación web, Cypress recomienda añadir un atributo «data-cy» en los elementos. Ejemplo: cy.get(‘[data-cy=submit]’).click().

6. Puedes utilizar el botón del visor en cypress para seleccionar elementos de una web y averiguar su jquery selector.

7. Al escribir los tests, utiliza un fichero de configuración para los parámetros (ejemplo: `/cypress/fixtures/data.json`).

8. En este ejemplo hay 3 ficheros de tests: `/cypress/integrations/login.spec.js, /cypress/integrations/buscar.spec.js, /cypress/integrations/comprar.spec.js, `.

## Pasos para instalar cypress en un proyecto

1. Inicializar el proyecto node. Comando: `npm init`

2. Instalar cypress: `npm install cypress`

3. Configurar la URL base en el fichero `cypress.json`

4. Escribir los tests en ficheros con extensión `*.spec.js`

5. Ejecutar cypress: `node_modules\.bin\cypress open`

## Enlaces de interés

<https://www.cypress.io/>

<https://javiercampos.es/blog/2019/05/14/testing-automatico-de-interfaz-grafica-en-aplicaciones-web-con-cypress/>